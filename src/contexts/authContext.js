import React, { createContext, useEffect, useReducer } from "react";

import firebase from "firebase/app";

import { reducer, initialState } from "./reducers/auth";

const AuthContext = createContext(null);

/**
 * rootState = {
 *  boards: { boards: [], lists: {} },
 *  ui: { selectedBoard: null }
 * }
 *
 *   boardContext.js:23 combineReducer - action RECEIVE_BOARDS
 *   boardContext.js:23 Reducer boards - action RECEIVE_BOARDS
 *   boardContext.js:23 Reducer ui - action RECEIVE_BOARDS
 */

export const AuthProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        // User is signed in.
        console.log("user detected");
        dispatch({
          type: "RECEIVE_USER",
          payload: {
            data: user,
          },
        });
      } else {
        // No user is signed in.
        console.log("no user");
        dispatch({
          type: "REMOVE_USER",
        });
      }
    });
  }, []);

  return (
    <AuthContext.Provider value={{ state, dispatch }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
