import React, { useEffect } from "react";
import useCompanies from "../../hooks/useCompanies";
import useUser from "../../hooks/useUser";

import CartBar from "../CartBar";
import BackHeader from "../BackHeader";

import { Link, useParams } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar, faHeart } from "@fortawesome/free-solid-svg-icons";
import { faHeart as faHeartRegular } from "@fortawesome/free-regular-svg-icons";

const Company = () => {
  const { selectors, actions } = useCompanies();
  const { selectors: userSelectors, actions: userActions } = useUser();

  let { id } = useParams();
  const company = selectors.getCompany(id);
  const services = selectors.getServices(company);

  useEffect(() => {
    actions.fetchServices(company);
    actions.fetchVotes(company);
    // eslint-disable-next-line
  }, [company]);

  const img = require(`../../assets/images/companies/${
    company.image ? company.image : "massage"
  }.jpg`);

  function addToFavorite() {
    userActions.setFavorite(company);
  }
  function removeToFavorite() {
    userActions.removeFavorite(company);
  }

  return (
    <>
      <BackHeader title={company.name} link="/" />
      <section className="page-content page-company">
        <section className="image-block">
          <img src={img} alt={company.name} />
          {company && userSelectors.isFavorite(company) ? (
            <button className="link fav" onClick={removeToFavorite}>
              <FontAwesomeIcon icon={faHeart} className="heart" />
            </button>
          ) : (
            <button className="link fav" onClick={addToFavorite}>
              <FontAwesomeIcon icon={faHeartRegular} className="heart" />
            </button>
          )}{" "}
        </section>
        <section className="content-block infos">
          <h1>{company.name}</h1>
          <div className="categories">
            {company.categories &&
              company.categories.map((category, index) => (
                <span key={index}>{category}</span>
              ))}
          </div>
          <div className="score">
            {selectors.getScore(company)}{" "}
            <FontAwesomeIcon icon={faStar} className="star" />
            <span className="muted">
              {" "}
              <Link to={"/company/" + company.id + "/comments"}>
                Voir tous les avis{" "}
              </Link>
            </span>
          </div>
        </section>
        <section className="content-block desc">
          <h3>Description</h3>
          <p>{company.desc}</p>
        </section>
        <section className="services">
          {services &&
            services.map((service, index) => (
              <Link
                className="service"
                to={"/company/" + company.id + "/service/" + service.id}
                key={index}
              >
                <div className="content">
                  <div className="title">{service.name}</div>
                  <div className="description">{service.description}</div>
                  <div className="price">{service.price}</div>
                </div>
              </Link>
            ))}
        </section>
      </section>
      <CartBar />
    </>
  );
};

export default Company;
