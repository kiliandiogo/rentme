export const initialState = {
  user: {},
  inputs: {},
};

/**
 * action = { type: String, payload: any }
 */
export const reducer = (state, action) => {
  switch (action.type) {
    case "RECEIVE_INPUT":
      return {
        ...state,
        inputs: {
          ...state.inputs,
          [action.payload.data.name]: action.payload.data.value,
        },
      };
    case "RECEIVE_USER":
      return {
        ...state,
        user: action.payload.data,
      };
    case "REMOVE_USER":
      return {
        ...state,
        user: {},
      };
    default:
      return state;
  }
};
