import React from "react";
import { useHistory } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";

const BackHeader = ({ title, link }) => {
  const history = useHistory();

  function goToLink() {
    history.push(link);
  }
  return (
    <header className="topbar">
      <button className="link" onClick={link ? goToLink : history.goBack}>
        <FontAwesomeIcon icon={faArrowLeft} size="lg" />
      </button>
      <div className="title">{title}</div>
    </header>
  );
};

export default BackHeader;
