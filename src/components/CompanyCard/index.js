import React from "react";
import { NavLink } from "react-router-dom";

import ImageLazyLoad from "../ImageLazyLoad";

const CompanyCard = ({ company }) => {
  /*const img = useMemo(
    () => require(`../../assets/images/companies/${company.image}.jpg`),
    [company.image]
  );*/

  if (!company) return <></>;

  return (
    <div className="company">
      <NavLink exact to={"/company/" + company.id}>
        <div className="image-block">
          {/*<img src={img} alt={company.name} />*/}
          <ImageLazyLoad
            url={require(`../../assets/images/companies/${company.image}.jpg`)}
            alt={"alt"}
            className={"image"}
          />
        </div>
        <h3>{company.name}</h3>
        <div className="categories">
          {company.categories.map((category, index) => (
            <span key={index}>{category}</span>
          ))}
        </div>
      </NavLink>
    </div>
  );
};

export default React.memo(CompanyCard);
