import React from "react";
import useAuth from "../../hooks/useAuth";

import { Link } from "react-router-dom";

const Login = () => {
  const { selectors, actions } = useAuth();

  const handleSubmit = (e) => {
    e.preventDefault();
    actions.login(selectors.getInput("email"), selectors.getInput("password"));
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    actions.setInput(name, value);
  };

  return (
    <div className="page-content page-login">
      <section className="content-block">
        <h2 className="text-center">Connectez vous !</h2>
        <form onSubmit={handleSubmit}>
          <div className="form-input">
            <label>Adresse mail</label>
            <input type="text" name="email" onChange={handleChange}></input>
          </div>

          <div className="form-input">
            <label>Mot de passe</label>
            <input type="text" name="password" onChange={handleChange}></input>
          </div>
          <button type="submit" className="btn btn-flex btn-valid">
            Connexion
          </button>

          <div className="subtext">
            Nouveau sur RentMe ? <Link to="/register">Créer un compte</Link>
          </div>
        </form>
      </section>
    </div>
  );
};

export default Login;
