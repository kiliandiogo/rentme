import "firebase/database";
import "firebase/firestore";
import firebase from "firebase/app";

export const fetchOrders = (user) =>
  firebase
    .firestore()
    .collection("order")
    .orderBy("timestamp", "desc")
    .where("uid", "==", user.uid)
    .get()
    .then((snapshot) =>
      snapshot.docs.map((doc) => {
        return { ...doc.data(), id: doc.id };
      })
    );

export const createOrder = (order) =>
  firebase
    .firestore()
    .collection("order")
    .add(order)
    .then((docRef) => {
      return { ...order, id: docRef.id };
    });
