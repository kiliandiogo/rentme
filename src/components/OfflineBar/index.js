import React from "react";

import useUI from "../../hooks/useUI";

const OfflineBar = () => {
  const { selectors } = useUI();

  if (selectors.isOnline()) return <></>;

  return (
    <div className="offlinebar">Vous n'êtes plus connecté à internet !</div>
  );
};

export default OfflineBar;
