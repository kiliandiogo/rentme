import React, { useEffect } from "react";
import useCompanies from "../../hooks/useCompanies";

import CartBar from "../CartBar";
import BackHeader from "../BackHeader";

import { useParams } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";

const CompanyComments = () => {
  const { selectors, actions } = useCompanies();

  let { id } = useParams();
  const company = selectors.getCompany(id);
  const votes = selectors.getVotes(company);

  useEffect(() => {
    actions.fetchVotes(company);
    // eslint-disable-next-line
  }, [company]);

  return (
    <>
      <BackHeader title={"Avis sur " + company.name} />
      <div className="page-content page-companycomments">
        <section className="score content-block border-bottom">
          <h3>{company.name}</h3>
          {(votes &&
            parseFloat(
              votes.reduce((acc, val) => acc + parseInt(val.score), 0)
            ) / votes.length) ||
            0}{" "}
          <FontAwesomeIcon className="star" icon={faStar} /> sur un total de{" "}
          {votes.length} avis
        </section>
        {votes &&
          votes.map((vote, index) => (
            <section className="comment content-block" key={index}>
              <div className="muted">
                {vote.score} <FontAwesomeIcon icon={faStar} className="star" />{" "}
                le {vote.date}
              </div>
              <div>{vote.comment}</div>
            </section>
          ))}
      </div>
      <CartBar />
    </>
  );
};

export default CompanyComments;
