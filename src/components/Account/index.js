import React from "react";

import { Link, useHistory } from "react-router-dom";

import firebase from "firebase";
import useAuth from "../../hooks/useAuth";

const Account = () => {
  const { user, actions } = useAuth();

  const history = useHistory();

  if (!user) {
    return <div className="page-content page-cart"></div>;
  }

  function handleChangeAvatar(e) {
    var file = e.target.files[0];

    // Get current username
    var user = firebase.auth().currentUser;

    // Create a Storage Ref w/ username
    var storageRef = firebase
      .storage()
      .ref("/profilePicture/" + user.uid + "/" + file.name);

    // Upload file
    var uploadTask = storageRef.put(file);

    uploadTask.on(
      "state_changed",
      function (snapshot) {
        // Observe state change events such as progress, pause, and resume
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log("Upload is " + progress + "% done");
        // eslint-disable-next-line
        switch (snapshot.state) {
          case firebase.storage.TaskState.PAUSED: // or 'paused'
            console.log("Upload is paused");
            break;
          case firebase.storage.TaskState.RUNNING: // or 'running'
            console.log("Upload is running");
            break;
        }
      },
      function (error) {
        // Handle unsuccessful uploads
      },
      function () {
        // Handle successful uploads on complete
        // For instance, get the download URL: https://firebasestorage.googleapis.com/...
        uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
          console.log("File available at", downloadURL);
          user
            .updateProfile({
              photoURL: downloadURL,
            })
            .then(function () {
              history.push("/account");
            })
            .catch(function (error) {
              // An error happened.
            });
        });
      }
    );
  }

  return (
    <div className="page-content page-account">
      <section className="avatar content-block">
        <label>
          <img alt="User Avatar" src={user.photoURL} />
          <input
            type="file"
            onChange={handleChangeAvatar}
            style={{ display: "none" }}
          ></input>
        </label>
      </section>
      <section className="infos content-block text-center bold">
        {user.email}
      </section>

      <section className="menu content-block">
        <Link to="/favorites" className="link muted">
          Mes favoris
        </Link>
        <button className="link muted" onClick={actions.logout}>
          Déconnexion
        </button>
      </section>
    </div>
  );
};

export default Account;
