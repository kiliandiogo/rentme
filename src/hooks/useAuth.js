import { useContext } from "react";
import AuthContext from "../contexts/authContext";

import { useHistory } from "react-router-dom";

import firebase from "firebase/app";

const useAuth = () => {
  const { state: authState, dispatch } = useContext(AuthContext);

  const history = useHistory();

  const actions = {
    setInput: (name, value) => {
      dispatch({
        type: "RECEIVE_INPUT",
        payload: {
          data: {
            name: name,
            value: value,
          },
        },
      });
    },
    register: (email, password) => {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        //make res asynchonous so that we can make grab the token before saving it.
        .then((res) => {
          console.log(res);
          history.push("/login");
        })
        .catch(console.log);
    },
    login: (email, password, setErrors, setToken) => {
      //change from create users to...
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        //everything is almost exactly the same as the function above
        .then((res) => {
          //const token = await Object.entries(res.user)[5][1].b;
          //set token to localStorage

          console.log(res);

          history.push("/account");
        })
        .catch((err) => {
          //setErrors((prev) => [...prev, err.message]);
        });
    },
    //no need for email and password
    logout: () => {
      // signOut is a no argument function
      firebase
        .auth()
        .signOut()
        .then((res) => {
          //remove the token
          localStorage.removeItem("user");

          history.push("/login");
        })
        .catch((err) => {
          //there shouldn't every be an error from firebase but just in case
          //setErrors((prev) => [...prev, err.message]);
          //whether firebase does the trick or not i want my user to do there thing.
          //localStorage.removeItem("token");
          //setToken(null);
          console.error(err.message);
        });
    },
  };

  const selectors = {
    getInput: (name) => authState.inputs[name] || "",
  };

  const user = firebase.auth().currentUser;

  return { selectors, actions, user };
};

export default useAuth;
