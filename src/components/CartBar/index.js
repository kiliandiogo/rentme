import React from "react";
import { Link } from "react-router-dom";

import useUser from "../../hooks/useUser";

const CartBar = () => {
  const { selectors } = useUser();
  const cartItems = selectors.getCartItems();

  if (cartItems.length <= 0) return <></>;

  return (
    <Link className="cartbar" to="/cart">
      <div className="price">
        {cartItems.length &&
          cartItems.reduce((acc, val) => parseFloat(acc + val.total_price), [])}
      </div>
      <div className="title">Afficher le panier</div>
      <div className="elements">{cartItems.length}</div>
    </Link>
  );
};

export default CartBar;
