import React from "react";
import useCompanies from "../../hooks/useCompanies";

import CompanyCard from "../CompanyCard";
import InstallPWA from "../InstallPWA";

const CompanyList = () => {
  const { selectors } = useCompanies();
  const companies = selectors.getCompanies();

  return (
    <div className="page-content">
      <InstallPWA />
      {companies &&
        companies.map((company, index) => (
          <CompanyCard key={company.id} company={company} />
        ))}
    </div>
  );
};

export default React.memo(CompanyList);
